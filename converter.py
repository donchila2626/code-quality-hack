import json


def lookup(severity):
    vv = {
        "Info": 'info',
        "Unknown": 'blocker',
        "Low": 'minor',
        "Medium": 'minor',
        "High": 'major',
        "Critical": 'critical'
    }
    return vv.get(severity, 'blocker')


if __name__ == '__main__':
    with open('security.json', 'r') as f:
        data = json.loads(f.read())

    result = []
    for vuln in data['vulnerabilities']:
        result.append(dict(
            description=vuln['description'],
            fingerprint=vuln['id'],
            severity=lookup(vuln['severity']),
            # location=dict(
            #     path=vuln[''],
            #     lines=dict(
            #         begin=vuln['']
            #     )
            # )
        ))
    with open('gl-code-quality-report.json', 'w') as f:
        f.write(json.dumps(result, indent=2))